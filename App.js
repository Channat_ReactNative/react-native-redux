/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import CounterApp from './src/components/CounterApp'

import {createStore} from 'redux'
import {Provider} from 'react-redux'


const initState = {
    counter: 0
}

const reducer = (state = initState, action) => {
    switch (action.type) {
        case 'INCREASE_COUNTER':
            return {counter: state.counter + 1}
        case 'DECREASE_COUNTER':
            return {counter: state.counter - 1}
    }
    return state
}

const store = createStore(reducer)

export default class App extends Component<Props> {


    render() {
        return (
            <Provider store={store}>
                <CounterApp />
            </Provider>
        );
    }
}

