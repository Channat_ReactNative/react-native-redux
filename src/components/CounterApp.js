import React, {Component} from 'react'
import {View, Text, Button, StyleSheet} from 'react-native'
import {connect} from 'react-redux'

class CounterApp extends Component {

     render() {
        return (
            <View style={styles.container}>
                <Button title={"Increase"} onPress={this.props.increaseCounter}/>
                <Text style={{fontSize: 20}}>{this.props.counter}</Text>
                <Button title={"Decrease"} onPress={this.props.decreaseCounter}/>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        counter: state.counter
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        increaseCounter: () => dispatch({type: 'INCREASE_COUNTER'}),
        decreaseCounter: () => dispatch({type: 'DECREASE_COUNTER'})
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (CounterApp)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        flexDirection: 'row',
    },
});
